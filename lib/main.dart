import 'package:flutter/material.dart';
import 'package:football/pages.dart';
import 'package:football/core/globals/constants/routes.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

void main() async {
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Football',
      themeMode: ThemeMode.light,
      theme: ThemeData(
        primaryColor: Colors.deepOrange,
      ),
      getPages: pages,
      initialRoute: Routes.home,
    );
  }
}
