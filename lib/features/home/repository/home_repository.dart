import '../model/fixture_model/fixture_model.dart';

abstract class HomeRepo {
  Future<List<FixtureModel>> getFixtures(
      {required String league, required String season});
}
