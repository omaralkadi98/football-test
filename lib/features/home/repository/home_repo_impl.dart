import 'package:football/core/globals/constants/api_routes.dart';
import 'package:football/core/globals/constants/constants.dart';
import 'package:football/core/globals/variables/variables.dart';
import 'package:football/features/home/model/fixture_model/fixture_model.dart';
import 'package:football/services/http/my_response.dart';
import 'package:get/get.dart';
import '../../../services/http/http_service.dart';
import '../../../services/http/http_service_impl.dart';
import 'home_repository.dart';

class HomeRepoImpl implements HomeRepo {
  late HttpService _httpService;

  HomeRepoImpl() {
    _httpService = Get.put(HttpServiceImpl());
  }

  bool get shouldGetNewRequest {
    String? dateString =
        storageContainer.read<String>(Constants.lastRequestDateKey);
    if (dateString == null) return true;
    DateTime? lastRequestDate = DateTime.parse(dateString);

    return DateTime.now().day != lastRequestDate.day;
  }

  @override
  Future<List<FixtureModel>> getFixtures(
      {required String league, required String season}) async {
    if (!shouldGetNewRequest) {
      List list = storageContainer.read(Constants.fixturesKey)!;
      return list.map((e) => FixtureModel.fromJson(e)).toList();
    } else {
      MyResponse res = await _httpService.getRequest(
        route: '${ApiRoutes.fixtures}?league=$league&season=$season',
        headers: {Constants.apiKey: Constants.apiKeyValue},
        isAuthorized: false,
      );

      if (res.statusCode == 200) {
        List list = res.object['response'];
        List<FixtureModel> fixtures =
            list.map((e) => FixtureModel.fromJson(e)).toList();
        await storageContainer.write(Constants.fixturesKey, fixtures);
        await storageContainer.write(
            Constants.lastRequestDateKey, DateTime.now().toString());
        return fixtures;
      }
      return [];
    }
  }
}
