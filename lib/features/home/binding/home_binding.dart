import '../controller/home_controller.dart';
import '../repository/home_repo_impl.dart';
import 'package:get/get.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(HomeRepoImpl());
    Get.put(HomeController(), permanent: true);
  }
}
