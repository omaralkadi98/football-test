import 'package:flutter/cupertino.dart';
import 'package:football/features/home/model/fixture_model/fixture_model.dart';
import 'package:football/features/home/model/fixture_status.dart';

import '../repository/home_repository.dart';
import '../repository/home_repo_impl.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  late HomeRepo _homePageRepo;

  RxBool isLoading = false.obs;
  RxInt tapIndex = 0.obs;
  RxList<FixtureModel> fixtures = <FixtureModel>[].obs;

  Rx<PageController> pageController = PageController().obs;

  List<FixtureModel> get finishedFixtures => fixtures
      .where((fixture) => FixtureStatus.finishedStatues
          .contains(fixture.fixture!.status!.short))
      .toList();

  List<FixtureModel> get upComingFixtures => fixtures
      .where((fixture) => FixtureStatus.upComingStatues
          .contains(fixture.fixture!.status!.short))
      .toList();

  @override
  Future<void> onInit() async {
    _homePageRepo = Get.find<HomeRepoImpl>();
    // pageController.value.addListener(() {
    //   tapIndex.value = pageController.value.page!.toInt();
    // });
    isLoading.toggle();
    fixtures.value =
        await _homePageRepo.getFixtures(league: '39', season: '2019');
    isLoading.toggle();

    super.onInit();
  }

  void changeTap(int index) {
    tapIndex.value = index;
    pageController.value.animateToPage(index,
        duration: const Duration(milliseconds: 500),
        curve: index == 0 ? Curves.easeIn : Curves.easeOut);
  }
}
