import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:football/features/home/model/fixture_model/fixture_model.dart';

class FixtureWidget extends StatelessWidget {
  const FixtureWidget({Key? key, required this.fixture, required this.onTap})
      : super(key: key);

  final FixtureModel fixture;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 8),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(25),
          boxShadow: const [
            BoxShadow(
              offset: Offset(1.5, 3),
              color: Color.fromARGB(255, 214, 214, 214),
              blurRadius: 5,
            ),
            // BoxShadow(
            //   offset: Offset(-1.5, -3),
            //   color: Color.fromARGB(255, 214, 214, 214),
            //   blurRadius: 4,
            // ),
          ],
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        fixture.teams!.home!.name!,
                        style: const TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(
                        width: 2,
                      ),
                      Image.network(
                        fixture.teams!.home!.logo!,
                        height: 35,
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 16),
                Column(
                  children: [
                    Text(
                      formatDate(DateTime.parse(fixture.fixture!.date!),
                          [HH, ':', nn]),
                      style: const TextStyle(
                        fontSize: 12,
                        color: Colors.red,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    const SizedBox(height: 4),
                    Text(
                      formatDate(
                          DateTime.parse(fixture.fixture!.date!), [d, ' ', M]),
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.grey[400],
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Row(
                    children: [
                      Image.network(
                        fixture.teams!.away!.logo!,
                        height: 35,
                      ),
                      const SizedBox(
                        width: 2,
                      ),
                      Text(
                        fixture.teams!.away!.name!,
                        style: const TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(height: 16),
            Text(
              '${fixture.goals!.home!} - ${fixture.goals!.away!}',
              style: const TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 16),
            Text(
              '${fixture.fixture!.venue!.name!} , ${fixture.fixture!.venue!.city!}',
              style: const TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
