import 'package:football/core/globals/constants/routes.dart';

import '../../../core/widgets/loading_widget.dart';
import '../controller/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../model/fixture_model/fixture_model.dart';
import 'widgets/fixture_widget.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<HomeController>();

    return Obx(
      () => Scaffold(
        backgroundColor: const Color.fromARGB(255, 247, 247, 247),
        appBar: AppBar(
          title: const Text('Football Games'),
          backgroundColor: Colors.deepPurple,
        ),
        bottomNavigationBar: _buildBottomBar(controller),
        body: controller.isLoading.isFalse
            ? PageView(
                controller: controller.pageController.value,
                children: [
                  _buildTap(controller.finishedFixtures),
                  _buildTap(controller.upComingFixtures),
                ],
              )
            : const LoadingWidget(),
      ),
    );
  }

  Widget _buildTap(List<FixtureModel> fixtures) {
    if (fixtures.isEmpty) {
      return const Center(
        child: Text(
          'No Matches!',
          style: TextStyle(
            color: Colors.deepPurple,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    }
    return ListView.builder(
      itemCount: fixtures.length,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: FixtureWidget(
            fixture: fixtures[index],
            onTap: () => Get.toNamed(
              Routes.matchDetails,
              arguments: fixtures[index].fixture!.id.toString(),
            ),
          ),
        );
      },
    );
  }

  Widget _buildBottomBar(HomeController controller) {
    if (controller.isLoading.isTrue) return const SizedBox();
    return Card(
      elevation: 4,
      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 8),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        // crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: InkWell(
              onTap: () {
                controller.changeTap(0);
              },
              child: Container(
                decoration: BoxDecoration(
                    color: controller.tapIndex.value == 0
                        ? Colors.deepPurple
                        : Colors.white,
                    borderRadius: const BorderRadius.horizontal(
                        left: Radius.circular(50))),
                child: Center(
                  heightFactor: 3,
                  child: Text(
                    'Finished',
                    style: TextStyle(
                      color: controller.tapIndex.value == 0
                          ? Colors.white
                          : Colors.deepPurple,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: () {
                controller.changeTap(1);
              },
              child: Container(
                decoration: BoxDecoration(
                    color: controller.tapIndex.value == 1
                        ? Colors.deepPurple
                        : Colors.white,
                    borderRadius: const BorderRadius.horizontal(
                        right: Radius.circular(50))),
                child: Center(
                  heightFactor: 3,
                  child: Text(
                    'Upcoming',
                    style: TextStyle(
                      color: controller.tapIndex.value == 1
                          ? Colors.white
                          : Colors.deepPurple,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
