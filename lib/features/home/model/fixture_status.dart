class FixtureStatus {
  static List<String> get finishedStatues => [ft, aet, pen];
  static List<String> get upComingStatues => [tbd, ns];

  static const String tbd = 'TBD';
  static const String ns = 'NS';
  static const String h1 = '1H';
  static const String ht = 'HT';
  static const String h2 = '2H';
  static const String et = 'ET';
  static const String p = 'P';
  static const String ft = 'FT';
  static const String aet = 'AET';
  static const String pen = 'PEN';
  static const String bt = 'BT';
  static const String susp = 'SUSP';
  static const String int = 'INT';
  static const String pst = 'PST';
  static const String canc = 'CANC';
  static const String awd = 'AWD';
  static const String wo = 'WO';
  static const String live = 'LIVE';
  static const String abd = 'ABD';
}
