class Goals {
  int? home;
  int? away;

  Goals({this.home, this.away});

  @override
  String toString() => 'Goals(home: $home, away: $away)';

  factory Goals.fromJson(Map<String, dynamic> json) => Goals(
        home: json['home'] as int?,
        away: json['away'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'home': home,
        'away': away,
      };

  Goals copyWith({
    int? home,
    int? away,
  }) {
    return Goals(
      home: home ?? this.home,
      away: away ?? this.away,
    );
  }
}
