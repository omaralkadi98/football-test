class Periods {
  int? first;
  int? second;

  Periods({this.first, this.second});

  @override
  String toString() => 'Periods(first: $first, second: $second)';

  factory Periods.fromJson(Map<String, dynamic> json) => Periods(
        first: json['first'] as int?,
        second: json['second'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'first': first,
        'second': second,
      };

  Periods copyWith({
    int? first,
    int? second,
  }) {
    return Periods(
      first: first ?? this.first,
      second: second ?? this.second,
    );
  }
}
