class Extratime {
  dynamic home;
  dynamic away;

  Extratime({this.home, this.away});

  @override
  String toString() => 'Extratime(home: $home, away: $away)';

  factory Extratime.fromJson(Map<String, dynamic> json) => Extratime(
        home: json['home'] as dynamic,
        away: json['away'] as dynamic,
      );

  Map<String, dynamic> toJson() => {
        'home': home,
        'away': away,
      };

  Extratime copyWith({
    dynamic home,
    dynamic away,
  }) {
    return Extratime(
      home: home ?? this.home,
      away: away ?? this.away,
    );
  }
}
