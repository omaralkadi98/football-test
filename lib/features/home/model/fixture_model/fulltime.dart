class Fulltime {
  int? home;
  int? away;

  Fulltime({this.home, this.away});

  @override
  String toString() => 'Fulltime(home: $home, away: $away)';

  factory Fulltime.fromJson(Map<String, dynamic> json) => Fulltime(
        home: json['home'] as int?,
        away: json['away'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'home': home,
        'away': away,
      };

  Fulltime copyWith({
    int? home,
    int? away,
  }) {
    return Fulltime(
      home: home ?? this.home,
      away: away ?? this.away,
    );
  }
}
