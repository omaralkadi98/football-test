class Venue {
  int? id;
  String? name;
  String? city;

  Venue({this.id, this.name, this.city});

  @override
  String toString() => 'Venue(id: $id, name: $name, city: $city)';

  factory Venue.fromJson(Map<String, dynamic> json) => Venue(
        id: json['id'] as int?,
        name: json['name'] as String?,
        city: json['city'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'city': city,
      };

  Venue copyWith({
    int? id,
    String? name,
    String? city,
  }) {
    return Venue(
      id: id ?? this.id,
      name: name ?? this.name,
      city: city ?? this.city,
    );
  }
}
