class Status {
  String? long;
  String? short;
  int? elapsed;

  Status({this.long, this.short, this.elapsed});

  @override
  String toString() {
    return 'Status(long: $long, short: $short, elapsed: $elapsed)';
  }

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        long: json['long'] as String?,
        short: json['short'] as String?,
        elapsed: json['elapsed'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'long': long,
        'short': short,
        'elapsed': elapsed,
      };

  Status copyWith({
    String? long,
    String? short,
    int? elapsed,
  }) {
    return Status(
      long: long ?? this.long,
      short: short ?? this.short,
      elapsed: elapsed ?? this.elapsed,
    );
  }
}
