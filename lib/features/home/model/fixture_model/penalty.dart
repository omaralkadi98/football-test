class Penalty {
  dynamic home;
  dynamic away;

  Penalty({this.home, this.away});

  @override
  String toString() => 'Penalty(home: $home, away: $away)';

  factory Penalty.fromJson(Map<String, dynamic> json) => Penalty(
        home: json['home'] as dynamic,
        away: json['away'] as dynamic,
      );

  Map<String, dynamic> toJson() => {
        'home': home,
        'away': away,
      };

  Penalty copyWith({
    dynamic home,
    dynamic away,
  }) {
    return Penalty(
      home: home ?? this.home,
      away: away ?? this.away,
    );
  }
}
