import 'fixture.dart';
import 'goals.dart';
import 'league.dart';
import 'score.dart';
import 'teams.dart';

class FixtureModel {
  Fixture? fixture;
  League? league;
  Teams? teams;
  Goals? goals;
  Score? score;

  FixtureModel({
    this.fixture,
    this.league,
    this.teams,
    this.goals,
    this.score,
  });

  @override
  String toString() {
    return 'FixtureModel(fixture: $fixture, league: $league, teams: $teams, goals: $goals, score: $score)';
  }

  factory FixtureModel.fromJson(Map<String, dynamic> json) => FixtureModel(
        fixture: json['fixture'] == null
            ? null
            : Fixture.fromJson(json['fixture'] as Map<String, dynamic>),
        league: json['league'] == null
            ? null
            : League.fromJson(json['league'] as Map<String, dynamic>),
        teams: json['teams'] == null
            ? null
            : Teams.fromJson(json['teams'] as Map<String, dynamic>),
        goals: json['goals'] == null
            ? null
            : Goals.fromJson(json['goals'] as Map<String, dynamic>),
        score: json['score'] == null
            ? null
            : Score.fromJson(json['score'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => {
        'fixture': fixture?.toJson(),
        'league': league?.toJson(),
        'teams': teams?.toJson(),
        'goals': goals?.toJson(),
        'score': score?.toJson(),
      };

  FixtureModel copyWith({
    Fixture? fixture,
    League? league,
    Teams? teams,
    Goals? goals,
    Score? score,
  }) {
    return FixtureModel(
      fixture: fixture ?? this.fixture,
      league: league ?? this.league,
      teams: teams ?? this.teams,
      goals: goals ?? this.goals,
      score: score ?? this.score,
    );
  }
}
