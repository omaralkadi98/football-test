class Home {
  int? id;
  String? name;
  String? logo;
  bool? winner;

  Home({this.id, this.name, this.logo, this.winner});

  @override
  String toString() {
    return 'Home(id: $id, name: $name, logo: $logo, winner: $winner)';
  }

  factory Home.fromJson(Map<String, dynamic> json) => Home(
        id: json['id'] as int?,
        name: json['name'] as String?,
        logo: json['logo'] as String?,
        winner: json['winner'] as bool?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'logo': logo,
        'winner': winner,
      };

  Home copyWith({
    int? id,
    String? name,
    String? logo,
    bool? winner,
  }) {
    return Home(
      id: id ?? this.id,
      name: name ?? this.name,
      logo: logo ?? this.logo,
      winner: winner ?? this.winner,
    );
  }
}
