class Halftime {
  int? home;
  int? away;

  Halftime({this.home, this.away});

  @override
  String toString() => 'Halftime(home: $home, away: $away)';

  factory Halftime.fromJson(Map<String, dynamic> json) => Halftime(
        home: json['home'] as int?,
        away: json['away'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'home': home,
        'away': away,
      };

  Halftime copyWith({
    int? home,
    int? away,
  }) {
    return Halftime(
      home: home ?? this.home,
      away: away ?? this.away,
    );
  }
}
