import 'package:football/features/match_details/model/line_up/line_up.dart';

import '../model/line_up/player.dart';
import '../repository/match_details_repository.dart';
import '../repository/match_details_repo_impl.dart';
import 'package:get/get.dart';

class MatchDetailsController extends GetxController {
  late MatchDetailsRepo _matchDetailsPageRepo;

  RxBool isLoading = false.obs;
  RxList<LineUp> linesUps = <LineUp>[].obs;

  List<List<Player>> get sortedHomePlayers => _formatePlayers(linesUps[0]);
  List<List<Player>> get sortedAwayPlayers => _formatePlayers(linesUps[1]);

  late String fixtureId;

  @override
  Future<void> onInit() async {
    _matchDetailsPageRepo = Get.find<MatchDetailsRepoImpl>();
    fixtureId = Get.arguments;
    isLoading.toggle();
    linesUps.value =
        await _matchDetailsPageRepo.getLineUp(fixtureId: fixtureId);
    isLoading.toggle();

    super.onInit();
  }

  List<List<Player>> _formatePlayers(LineUp lineUp) {
    List<List<Player>> playersRows = [];

    for (Player p in lineUp.startXi!.map<Player>((e) => e.player!).toList()) {
      int row = int.parse(p.grid![0]);
      int col = int.parse(p.grid![2]);

      if (playersRows.length < row) {
        playersRows.add([]);
      }
      playersRows[row - 1].insert(0, p);
    }
    return playersRows;
  }
}
