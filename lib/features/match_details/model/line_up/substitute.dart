import 'player.dart';

class Substitute {
  Player? player;

  Substitute({this.player});

  @override
  String toString() => 'Substitute(player: $player)';

  factory Substitute.fromJson(Map<String, dynamic> json) => Substitute(
        player: json['player'] == null
            ? null
            : Player.fromJson(json['player'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => {
        'player': player?.toJson(),
      };
}
