class Team {
  int? id;
  String? name;
  String? logo;
  dynamic colors;

  Team({this.id, this.name, this.logo, this.colors});

  @override
  String toString() {
    return 'Team(id: $id, name: $name, logo: $logo, colors: $colors)';
  }

  factory Team.fromJson(Map<String, dynamic> json) => Team(
        id: json['id'] as int?,
        name: json['name'] as String?,
        logo: json['logo'] as String?,
        colors: json['colors'] as dynamic,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'logo': logo,
        'colors': colors,
      };
}
