import 'package:football/features/match_details/model/line_up/line_up.dart';

abstract class MatchDetailsRepo {
  Future<List<LineUp>> getLineUp({required String fixtureId});
}
