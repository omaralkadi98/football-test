import 'package:football/core/globals/constants/api_routes.dart';
import 'package:football/core/globals/constants/constants.dart';
import 'package:football/features/match_details/model/line_up/line_up.dart';
import 'package:football/services/http/my_response.dart';
import 'package:get/get.dart';
import '../../../services/http/http_service.dart';
import '../../../services/http/http_service_impl.dart';
import 'match_details_repository.dart';

class MatchDetailsRepoImpl implements MatchDetailsRepo {
  late HttpService _httpService;

  MatchDetailsRepoImpl() {
    _httpService = Get.put(HttpServiceImpl());
  }

  @override
  Future<List<LineUp>> getLineUp({required String fixtureId}) async {
    MyResponse res = await _httpService.getRequest(
      route: '${ApiRoutes.linesUps}?fixture=$fixtureId',
      headers: {Constants.apiKey: Constants.apiKeyValue},
      isAuthorized: false,
    );

    if (res.statusCode == 200) {
      List list = res.object['response'];
      return list.map((e) => LineUp.fromJson(e)).toList();
    }
    return [];
  }
}
