import 'package:football/core/widgets/loading_widget.dart';
import 'package:football/features/match_details/model/line_up/line_up.dart';

import '../controller/match_details_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../model/line_up/player.dart';

class MatchDetailsView extends StatelessWidget {
  const MatchDetailsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<MatchDetailsController>();

    return Obx(
      () => Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          iconTheme: const IconThemeData(color: Colors.deepPurple),
        ),
        body: _buildBody(controller),
      ),
    );
  }

  Widget _buildBody(MatchDetailsController controller) {
    if (controller.isLoading.isTrue) return const LoadingWidget();
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      children: [
        Row(
          children: [
            Expanded(
              child: _buildTeam(controller.linesUps[0]),
            ),
            const SizedBox(width: 16),
            const SizedBox(width: 16),
            Expanded(
              child: _buildTeam(controller.linesUps[1]),
            ),
          ],
        ),
        const SizedBox(height: 24),
        ..._buildTeamLinesup(controller.sortedHomePlayers),
        const SizedBox(height: 16),
        ..._buildTeamLinesup(controller.sortedAwayPlayers).reversed,
      ],
    );
  }

  Column _buildTeam(LineUp lineUp) {
    return Column(
      children: [
        Image.network(
          lineUp.team!.logo!,
          height: 60,
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          lineUp.team!.name!,
          style: const TextStyle(fontSize: 14),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          lineUp.formation!,
          style: const TextStyle(fontSize: 14),
        ),
      ],
    );
  }

  List<Widget> _buildTeamLinesup(List<List<Player>> playersRows) {
    return [
      for (List<Player> row in playersRows)
        Row(
          mainAxisAlignment: row.length > 1
              ? MainAxisAlignment.spaceBetween
              : MainAxisAlignment.center,
          children: [
            for (Player p in row)
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16),
                child: _buildPlayer(p),
              )
          ],
        ),
    ];
  }

  Widget _buildPlayer(Player p) {
    return Column(
      children: [
        Text(
          p.number.toString(),
          style: const TextStyle(fontSize: 20),
        ),
        const SizedBox(height: 8),
        Text(p.name!.split(' ').last),
      ],
    );
  }
}
