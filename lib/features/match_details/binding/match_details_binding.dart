import '../controller/match_details_controller.dart';
import '../repository/match_details_repo_impl.dart';
import 'package:get/get.dart';

class MatchDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(MatchDetailsRepoImpl());
    Get.put(MatchDetailsController());
  }
}
