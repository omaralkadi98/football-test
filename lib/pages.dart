import 'package:football/core/globals/constants/routes.dart';
import 'package:football/features/home/binding/home_binding.dart';
import 'package:football/features/home/view/home_view.dart';
import 'package:football/features/match_details/binding/match_details_binding.dart';
import 'package:football/features/match_details/view/match_details_view.dart';
import 'package:get/get.dart';

List<GetPage> get pages => [
      GetPage(
        name: Routes.home,
        page: () => const HomeView(),
        binding: HomeBinding(),
      ),
      GetPage(
        name: Routes.matchDetails,
        page: () => const MatchDetailsView(),
        binding: MatchDetailsBinding(),
        fullscreenDialog: true,
        transition: Transition.downToUp,
      ),
    ];
