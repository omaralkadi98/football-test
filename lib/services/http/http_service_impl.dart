import 'dart:async';
import 'dart:convert';
import 'dart:io';

import '../../core/globals/constants/constants.dart';
import '../../core/globals/variables/variables.dart';
import '../../core/widgets/error_snackbar.dart';
import 'exception_handler.dart';
import 'http_service.dart';
import 'package:http/http.dart';

import 'my_response.dart';

class HttpServiceImpl implements HttpService {
  final ExceptionHandler _exceptionHandler = ExceptionHandler();

  @override
  Future<MyResponse> getRequest(
      {required String route,
      required Map<String, String> headers,
      required bool isAuthorized}) async {
    late Response response;
    Request request = Request('GET', Uri.parse(Constants.baseUrl + route));
    var data;

    try {
      request.headers
          .addAll({'Content-Type': 'application/json', 'accept': '*/*'});
      request.headers.addAll(headers);
      if (isAuthorized) {
        request.headers.addAll({
          'X-CSRF-TOKEN': '',
          'Authorization': 'Bearer ${storageContainer.read('user')['token']}'
        });
      }
      print('GET | ${Constants.baseUrl}$route');
      response = await Response.fromStream(await request.send())
          .timeout(const Duration(seconds: Constants.timeout));
      data = jsonDecode(response.body);
      print('${response.statusCode} | ${response.body}');
      if (response.statusCode != 200 && response.statusCode != 201) {
        errorSnackBar(data['message']);
        return MyResponse(response.statusCode, data);
      } else {
        return MyResponse(200, data);
      }
    } on TimeoutException catch (e) {
      return _exceptionHandler.timeoutHandler(route: route);
    } on SocketException catch (e) {
      return _exceptionHandler.socketExceptionHandler(route: route);
    } catch (e) {
      print(e);
      print(response.body);
      if (response.body == 'Unauthorized.') {
        _exceptionHandler.unAuthExceptionHandler();
      }
      return MyResponse(response.statusCode, {});
    }
  }

  @override
  Future<MyResponse> postRequest(
      {required String route,
      required Map<String, String> data,
      required Map<String, String> headers,
      required bool isAuthorized,
      bool isDelete = false}) async {
    String method = 'POST';

    if (isDelete) {
      method = 'DELETE';
    }

    late Response response;
    MultipartRequest request =
        MultipartRequest(method, Uri.parse(Constants.baseUrl + route));

    try {
      request.headers.addAll({
        'Content-Type': 'application/json',
        'accept': '*/*',
      });
      request.headers.addAll(headers);
      if (isAuthorized) {
        request.headers.addAll({
          'Authorization': 'Bearer ${storageContainer.read('user')['token']}'
        });
      }
      request.fields.addAll(data);
      print(request.fields);
      print('$method | ${Constants.baseUrl}$route');
      response = await Response.fromStream(await request.send())
          .timeout(const Duration(seconds: Constants.timeout));
      final parsedResponse = jsonDecode(response.body);
      print('${response.statusCode} | ${response.body}');
      if (response.statusCode != 200 && response.statusCode != 201) {
        errorSnackBar(parsedResponse['message']);
        return MyResponse(response.statusCode, parsedResponse);
      } else {
        return MyResponse(200, parsedResponse);
      }
    } on TimeoutException catch (e) {
      return _exceptionHandler.timeoutHandler(route: route);
    } on SocketException catch (e) {
      return _exceptionHandler.socketExceptionHandler(route: route);
    } catch (e) {
      print(e);
      print(response.body);
      if (response.body == 'Unauthorized.') {
        _exceptionHandler.unAuthExceptionHandler();
      }
      return MyResponse(response.statusCode, {});
    }
  }
}
