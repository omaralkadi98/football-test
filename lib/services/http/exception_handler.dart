import 'package:get/get.dart';
import '../../core/globals/variables/variables.dart';
import 'my_response.dart';
import '../../core/widgets/error_snackbar.dart';

class ExceptionHandler {
  MyResponse timeoutHandler({required String route}) {
    print('Connection Timeout | $route');
    errorSnackBar('Connection timeout');
    return MyResponse(-3, {});
  }

  MyResponse socketExceptionHandler({required String route}) {
    print('No internet connection | $route');
    errorSnackBar('No internet connection');
    return MyResponse(-5, {});
  }

  void unAuthExceptionHandler() {
    storageContainer.remove('user');
    errorSnackBar('Unauthorized, please login and try again');
    Get.offNamedUntil('/welcome', (route) => false);
  }
}
