import 'my_response.dart';

abstract class HttpService {
  Future<MyResponse> getRequest(
      {required String route,
      required Map<String, String> headers,
      required bool isAuthorized});

  Future<MyResponse> postRequest(
      {required String route,
      required Map<String, String> data,
      required Map<String, String> headers,
      required bool isAuthorized,
      bool isDelete});
}
