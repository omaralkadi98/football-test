import 'package:flutter/material.dart';
import 'package:get/get.dart';

void errorSnackBar(String error) {
  Get.snackbar('Error', error,
      backgroundColor: Colors.red,
      snackPosition: SnackPosition.BOTTOM,
      snackStyle: SnackStyle.FLOATING,
      colorText: Colors.white);
}
