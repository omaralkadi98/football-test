import 'package:flutter/material.dart';
import 'package:get/get.dart';

void successSnackBar(String message) {
  Get.snackbar('Success', message,
      backgroundColor: Colors.green,
      snackPosition: SnackPosition.BOTTOM,
      snackStyle: SnackStyle.FLOATING,
      colorText: Colors.white);
}
